# Terraform Workflows

Some Gitlab patterns that work well for Terraform child pipelines

## Basic

Consumption of the basic resource will look something like this:

```
unique-name:
  stage: Dispatch
  variables:
    PROJECT_NAME: unique-name
    PROJECT_PATH: path/to/terraform
  trigger:
    include: ["https://gitlab.com/scaleoutllc/gitlab-ci-utils/-/raw/main/terraform/basic.yml"]
    strategy: depend
    forward:
      pipeline_variables: true
  rules:
    - changes: [$PROJECT_PATH/**/*]
```

Note that pipeline variables are forwarded in the above configuration allowing consumers to override things like the 
image terraform jobs will use at a high level and not per job. Things like AWS access and secret keys should be overriden 
on a per-job basis.